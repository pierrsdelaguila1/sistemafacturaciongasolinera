package EmpresaCombustible;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public class BaseDatosCliente {
    private BufferedReader br;
    ArrayList<Cliente> cliente;

    public BaseDatosCliente() {
        cliente = new ArrayList<>();
    }

    public BaseDatosCliente(BufferedReader br, ArrayList<Cliente> cliente) {
        this.cliente = cliente;
        this.br = br;
    }

    public void mostrarDatosCliente() {
        System.out.println("\nBASE DE DATOS DE CLIENTES ");
        System.out.println("-----------------------------");
        for (Cliente clientes : cliente) {
            System.out.println(clientes.reporteCliente());
        }
    }

    public Cliente agregarCliente(String nombreCliente, String clienteEdad, int clienteCompro) throws IOException {
        System.out.print("ingresar su nombre: ");
        nombreCliente = br.readLine();
        System.out.print("Ingresar la edad: ");
        clienteEdad = br.readLine();
        System.out.print("Ingresar la cantidad de compra realizada: ");
        clienteCompro = Integer.parseInt(br.readLine());
        System.out.println("Los datos han sido agregados...\n");
        return new Cliente(nombreCliente, clienteEdad, clienteCompro);
    }

    public void eliminarCLiente(String nombreClienteEliminar) throws IOException {
        Iterator<Cliente> iterator = cliente.iterator();
        boolean eliminarCLiente = false;
        while (iterator.hasNext()) {
            Cliente clientes = iterator.next();
            if (clientes.getNombreCliente().equalsIgnoreCase(nombreClienteEliminar)) {
                iterator.remove();
                eliminarCLiente = true;
            }
        }
        if (eliminarCLiente == false) {
            System.out.println("El nombre del cliente no existe");
        }
    }

    public void modificarCliente(String nombreClienteModficar, String nombreCliente, String clienteEdad, int codigoCliente) throws IOException {
        boolean encontrarClienteModificar = false;
        for (Cliente clienteModificado : cliente) {
            if (clienteModificado.getNombreCliente().equalsIgnoreCase(nombreClienteModficar)) {
                System.out.print("ingresar su nombre: ");
                nombreCliente = br.readLine();
                System.out.print("Ingresar la edad del ciente: ");
                clienteEdad = br.readLine();
                System.out.print("Ingresar el codigo del cliente: ");
                codigoCliente = Integer.parseInt(br.readLine());
                System.out.println("Los datos han sido modificados..\n");
                encontrarClienteModificar = true;
                clienteModificado.setNombreCliente(nombreCliente);
                clienteModificado.setClienteEdad(clienteEdad);
                clienteModificado.setCodigoCliente(codigoCliente);
            }
        }
        if (encontrarClienteModificar == false) {
            System.out.println("El nombre de cliente que quiere modifcar no existe ");
        }
    }

    public void buscarCliente(String clienteBuscar) {
        boolean encontrarCliente = false;
        for (Cliente buscarCliente : cliente) {
            if (buscarCliente.getNombreCliente().equalsIgnoreCase(clienteBuscar)) {
                System.out.println("\n");
                System.out.println("El nombre del cliente es     :  " + buscarCliente.getNombreCliente());
                System.out.println("La edad del cliente es       : " + buscarCliente.getClienteEdad());
                System.out.println("El codigo del cliente es     : " + buscarCliente.getCodigoCliente());
                encontrarCliente = true;
            }
        }
        if (encontrarCliente == false) {
            System.out.println("El  nombre del cliente no existe");
        }
    }

    public Cliente buscarClientePorCodigo(int codigoCliente) {
        Cliente clienteEncontrado = null;

        for (Cliente buscarCliente : cliente) {
            if (buscarCliente.getCodigoCliente() == codigoCliente) {
                clienteEncontrado = buscarCliente;
            }
        }

        return clienteEncontrado;

    }
}

package EmpresaCombustible;

import java.io.BufferedReader;
import java.io.IOException;

public class menuOpciones {
    private BufferedReader br;

    public menuOpciones(BufferedReader br) {
        this.br = br;
    }

    public int menu() throws IOException {
        int opcion;
        do {
            System.out.println("\n\n----------------------------------------");
            System.out.println("Sistema de facturación de una gasolinera");
            System.out.println("------------------------------------------\n");

            System.out.println("1)- Registrar las estaciones de combustible de la empresa ");
            System.out.println("2)-Ingresar al inventario de estaciones que tiene la empresa ");
            System.out.println("3)-Registrar clientes ");
            System.out.println("4)-Ingresar al inventario del cliente");
            System.out.println("5)-Registro de ventas ");
            System.out.print("elige una de la cinco opciones: ");
            opcion = Integer.parseInt(br.readLine());

        } while (opcion < 1 || opcion > 6);
        return opcion;
    }


}
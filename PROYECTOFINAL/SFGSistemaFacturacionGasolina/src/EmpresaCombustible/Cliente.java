package EmpresaCombustible;

public class Cliente {
    private int codigoCliente;
    private String nombreCliente;
    private String clienteEdad;
    private int compraRealizada;

    public Cliente() {
        nombreCliente = "";
        clienteEdad = "";
        codigoCliente = 0;
    }

    public Cliente(String nombreCliente, String clienteEdad, int codigoCliente) {
        this.nombreCliente = nombreCliente;
        this.clienteEdad = clienteEdad;
        this.codigoCliente = codigoCliente;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getClienteEdad() {
        return clienteEdad;
    }

    public void setClienteEdad(String clienteEdad) {
        this.clienteEdad = clienteEdad;
    }

    public int getCompraRealizada() {
        return compraRealizada;
    }

    public void setCompraRealizada(int compraRealizada) {
        this.compraRealizada = compraRealizada;
    }

    public int getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(int codigoCliente) {
        this.codigoCliente = codigoCliente;
    }


    public String reporteCliente() {
        return "\tnombre del cliente     es:       -->" + nombreCliente + "\n" +
                "\tLa edad del cliente   es:       --> " + clienteEdad + "\n" +
                "\tEl codigo del cliente es:       -->" + codigoCliente + "\n" +
                "======================================================" + "\n";
    }
}

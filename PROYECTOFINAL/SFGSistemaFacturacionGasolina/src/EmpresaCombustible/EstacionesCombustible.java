package EmpresaCombustible;

public class EstacionesCombustible {
    private String nombreEstacion;
    private String tipoCombustible;
    private int cantidadCombustible;
    private int precioCombustible;


    public EstacionesCombustible() {
        nombreEstacion = "";
        tipoCombustible = "";
        cantidadCombustible = 0;
        precioCombustible = 0;

    }

    public EstacionesCombustible(String nombreEstacion, String tipoCombustible, int precioCombustible, int cantidadCombustible) {
        this.nombreEstacion = nombreEstacion;
        this.tipoCombustible = tipoCombustible;
        this.cantidadCombustible = cantidadCombustible;
        this.precioCombustible = precioCombustible;

    }


    public String getNombreEstacion() {
        return nombreEstacion;
    }

    public void setNombreEstacion(String nombreEstacion) {
        this.nombreEstacion = nombreEstacion;
    }

    public String getTipoCombustible() {
        return tipoCombustible;
    }

    public void setTipoCombustible(String tipoCombustible) {
        this.tipoCombustible = tipoCombustible;
    }

    public int getCantidadCombustible() {
        return cantidadCombustible;
    }

    public void setCantidadCombustible(int cantidadCombustible) {
        this.cantidadCombustible = cantidadCombustible;
    }

    public int getPrecioCombustible() {
        return precioCombustible;
    }

    public void setPrecioCombustible(int precioCombustible) {
        this.precioCombustible = precioCombustible;
    }

    public String reporteEstacionCombustible() {
        return "\tnombre de la estación       -->" + nombreEstacion + "\n" +
                "\tTipo de combustible        --> " + tipoCombustible + "\n" +
                "\tel precio del combustible  --> " + precioCombustible + "\n" +
                "\tcantidad Combutible        -->" + cantidadCombustible + "\n" +
                "======================================================" + "\n";
    }

}

package EmpresaCombustible;

import jdk.management.resource.internal.inst.FileOutputStreamRMHooks;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public class Inventario {
    private BufferedReader br;
    ArrayList<EstacionesCombustible> estacionesCombustibles;

    public Inventario() {
        estacionesCombustibles = new ArrayList<>();
    }

    public Inventario(BufferedReader br, ArrayList<EstacionesCombustible> estacionesCombustibles) {
        this.estacionesCombustibles = estacionesCombustibles;
        this.br = br;
    }

    public EstacionesCombustible agregarInventario(String nombreEstacion, String tipoCombustible, int precioCombutible, int cantidadCombustible) throws IOException {

        System.out.print("\tIngresar nombre de la estacion: ");
        nombreEstacion = br.readLine();

        do {
            System.out.println("\tTipo combustible: ");
            System.out.println("A) Carbon    ");
            System.out.println("B) Petroleo   ");
            System.out.println("C) Gas Natural");
            System.out.print("Elegir una de las 3 opcines (A-B-C): ");
            tipoCombustible = br.readLine();
        } while (!tipoCombustible.equalsIgnoreCase("A") && !tipoCombustible.equalsIgnoreCase("B") && !tipoCombustible.equalsIgnoreCase("C"));
        if (tipoCombustible.equalsIgnoreCase("A")) {
            tipoCombustible = "Carbon";
            precioCombutible = 10;
        } else if (tipoCombustible.equalsIgnoreCase("B")) {
            tipoCombustible = "Petroleo";
            precioCombutible = 20;
        } else {
            tipoCombustible = "Gas Natural";
            precioCombutible = 30;
        }
        System.out.print("\tIngresar la cantidad combustible: ");
        cantidadCombustible = Integer.parseInt(br.readLine());
        System.out.println("Los datos han sido agregados...\n");


        return new EstacionesCombustible(nombreEstacion, tipoCombustible, precioCombutible, cantidadCombustible);
    }

    public void mostrarInventario() {
        System.out.println("\nEMPRESA OF THE EAGLE");
        System.out.println("================================================ ");
        for (EstacionesCombustible estaciones : estacionesCombustibles) {
            System.out.println(estaciones.reporteEstacionCombustible());
        }
    }

    public void eliminarEstacionCombustible(String nombreEstacionEliminar) throws IOException {
        Iterator<EstacionesCombustible> iterator = estacionesCombustibles.iterator();
        boolean eliminarEstacion = false;
        while (iterator.hasNext()) {
            EstacionesCombustible estacion = iterator.next();
            if (estacion.getNombreEstacion().equalsIgnoreCase(nombreEstacionEliminar)) {
                iterator.remove();
                eliminarEstacion = true;
            }
        }
        if (eliminarEstacion == false) {
            System.out.println("el nombre de la estacion no existe");
        }
    }

    public void modficarEstacionCombustible(String nombreEstacionModificar, String nombreEstacion, String tipoCombustible, int precioCombustible, int cantidadCombustible) throws IOException {
        boolean encontrarEstacionModicar = false;
        for (EstacionesCombustible estacionModificada : estacionesCombustibles) {
            if (estacionModificada.getNombreEstacion().equalsIgnoreCase(nombreEstacionModificar)) {
                System.out.print("\tIngresar nombre de la estacion: ");
                nombreEstacion = br.readLine();
                do {
                    System.out.println("\tTipo combustible: ");
                    System.out.println("A) Carbon      || 10 soles   ");
                    System.out.println("B) Petroleo    || 20 soles  ");
                    System.out.println("C) Gas Natural || 30 coles");
                    System.out.print("Elegir una de las 3 opcines (A-B-C): ");
                    tipoCombustible = br.readLine();
                } while (!tipoCombustible.equalsIgnoreCase("A") && !tipoCombustible.equalsIgnoreCase("B") && !tipoCombustible.equalsIgnoreCase("C"));
                if (tipoCombustible.equalsIgnoreCase("A")) {
                    tipoCombustible = "Carbon";
                    precioCombustible = 10;
                } else if (tipoCombustible.equalsIgnoreCase("B")) {
                    tipoCombustible = "Petroleo";
                    precioCombustible = 20;
                } else {
                    tipoCombustible = "Gas Natural";
                    precioCombustible = 30;
                }
                System.out.print("\tIngresar la cantidad combustible: ");
                cantidadCombustible = Integer.parseInt(br.readLine());
                System.out.println("Los datos han sido modicados...\n");
                encontrarEstacionModicar = true;
                estacionModificada.setNombreEstacion(nombreEstacion);
                estacionModificada.setTipoCombustible(tipoCombustible);
                estacionModificada.setPrecioCombustible(precioCombustible);
                estacionModificada.setCantidadCombustible(cantidadCombustible);
            }
        }
        if (encontrarEstacionModicar == false) {
            System.out.println("El nombre que quiere modificar no existe ");
        }
    }

    public void BuscarEstacion(String estacionBuscar) {

        boolean encontrarEstacion = false;
        for (EstacionesCombustible estacion : estacionesCombustibles) {
            if (estacion.getNombreEstacion().equalsIgnoreCase(estacionBuscar)) {
                System.out.println("\n");
                System.out.println("El nombre de la estacion es   :  " + estacion.getNombreEstacion());
                System.out.println("El tipo de combustible es     : " + estacion.getTipoCombustible());
                System.out.println("El el precio del combustible  : " + estacion.getPrecioCombustible());
                System.out.println("La cantidad de combustible es : " + estacion.getCantidadCombustible());
                encontrarEstacion = true;

            }


        }
        if (encontrarEstacion == false) {
            System.out.println("El nombre de la estacion que esta buscando no sé encuentra");

        }


    }

    public EstacionesCombustible BuscarEstacion2(String estacionBuscar) {
        EstacionesCombustible estacionesCombustible = null;

        for (EstacionesCombustible estacion : estacionesCombustibles) {
            if (estacion.getNombreEstacion().equalsIgnoreCase(estacionBuscar)) {
                estacionesCombustible = estacion;
                break;
            }
        }

        return estacionesCombustible;
    }

    public boolean validaStock(EstacionesCombustible combustibleEncontrado, int cantidadCombustuble) {

        if (combustibleEncontrado.getCantidadCombustible() <= 0) {
            return false;
        }

        if (cantidadCombustuble > combustibleEncontrado.getCantidadCombustible()) {
            return false;
        }

        actualizarStockInventario(combustibleEncontrado, cantidadCombustuble);

        return true;
    }

    public void actualizarStockInventario(EstacionesCombustible combustible, int cantidadcombustible) {

        EstacionesCombustible nuevoestacionesCombustible = new EstacionesCombustible();
        for (EstacionesCombustible estacion : estacionesCombustibles) {
            if (estacion.getNombreEstacion().equalsIgnoreCase(combustible.getNombreEstacion())) {

                int calculaNuevoCantidad = estacion.getCantidadCombustible() - cantidadcombustible;

                nuevoestacionesCombustible = estacion;
                nuevoestacionesCombustible.setCantidadCombustible(calculaNuevoCantidad);

                estacionesCombustibles.remove(estacion);
                estacionesCombustibles.add(nuevoestacionesCombustible);
                break;
            }
        }

    }
}




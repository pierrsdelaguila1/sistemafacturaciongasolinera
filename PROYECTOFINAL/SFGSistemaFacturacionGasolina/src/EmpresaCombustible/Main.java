package EmpresaCombustible;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;


public class Main {

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int opcion;
        String nombreEstacion = "";
        String tipoCombustible = "";
        int cantidadCombustible = 0;
        int numerEstacionCombustible = 0;
        int opcionInventario;
        int intentarNuevo = 0;
        String nombreEstacionEliminar = "";
        int numeroClientes = 0;
        String clienteEdad = "";
        String nombreCliente = "";
        int clienteCodigo = 0;
        String nombreEstacionModificar = "";
        String nombreBuscar = "";
        int opcionVenta = 0;
        int opcionCliente = 0;
        int entradaCliente = 0;
        String nombreClienteEliminar = "";
        String nombreClienteModificar = "";
        String clienteBuscar = "";
        int entradaVenta = 0;
        int precioCombustible = 0;
        int codigoClienteBuscar = 0;
        String nombreCombustibleBuscar = "";
        String tipocomprobante = "";


        int cantidadCombustibleBuscar = 0;
        int diaventa = 0;
        ArrayList<EstacionesCombustible> estacionesCombustibles = new ArrayList<EstacionesCombustible>();
        ArrayList<Cliente> cliente = new ArrayList<Cliente>();
        ArrayList<VentaCombustible> ventas = new ArrayList<VentaCombustible>();
        menuOpciones menu = new menuOpciones(br);
        do {
            opcion = menu.menu();

            switch (opcion) {
                case 1:
                    do {
                        System.out.println();
                        System.out.print("¿Cuántas estaciones de combustible tiene la empresa?: ");
                        numerEstacionCombustible = Integer.parseInt(br.readLine());
                    } while (numerEstacionCombustible <= 0);
                    for (int contadorEstacion = 1; contadorEstacion <= numerEstacionCombustible; contadorEstacion++) {
                        System.out.println("\nEstación Combustible #" + contadorEstacion + "\n");
                        System.out.print("\tIngresar nombre de la estacion: ");
                        nombreEstacion = br.readLine();
                        do {
                            System.out.println("\tTipo combustible y el costo: ");
                            System.out.println("A) Carbon      || 10 soles    ");
                            System.out.println("B) Petroleo    || 20 soles    ");
                            System.out.println("C) Gas Natural || 30 soles   ");
                            System.out.print("Elegir una de las 3 opcines (A-B-C): ");
                            tipoCombustible = br.readLine();
                        } while (!tipoCombustible.equalsIgnoreCase("A") && !tipoCombustible.equalsIgnoreCase("B") && !tipoCombustible.equalsIgnoreCase("C"));

                        if (tipoCombustible.equalsIgnoreCase("A")) {
                            tipoCombustible = "Carbon";
                            precioCombustible = 10;

                        } else if (tipoCombustible.equalsIgnoreCase("B")) {
                            tipoCombustible = "Petroleo";
                            precioCombustible = 20;
                        } else {
                            tipoCombustible = "Gas Natural";
                            precioCombustible = 30;
                        }
                        System.out.print("\tIngresar la cantidad combustible: ");
                        cantidadCombustible = Integer.parseInt(br.readLine());

                        EstacionesCombustible estacionesCombustible = new EstacionesCombustible(nombreEstacion, tipoCombustible, precioCombustible, cantidadCombustible);

                        estacionesCombustibles.add(estacionesCombustible);
                    }
                    break;

                case 2:
                    Inventario inventario = new Inventario(br, estacionesCombustibles);
                    do {
                        System.out.println("\n");
                        System.out.println("\t1) Mostrar Inventario ");
                        System.out.println("\t2) Agregar Inventario ");
                        System.out.println("\t3) Eliminar inventario ");
                        System.out.println("\t4) Modificar inventario");
                        System.out.println("\t5) Buscar estacion");
                        System.out.print("\tElige un de las opciones (1-2-3-4-5): ");
                        opcionInventario = Integer.parseInt(br.readLine());
                    } while (opcionInventario <= 0);
                    if (opcionInventario != 1 && opcionInventario != 2 && opcionInventario != 3 && opcionInventario != 4 && opcionInventario != 5) {
                        do {
                            System.out.print("Opción incorrecta. Ingresar de nuevo: ");
                            intentarNuevo = Integer.parseInt(br.readLine());

                        } while (intentarNuevo <= 0 || intentarNuevo > 5);
                    }
                    if (opcionInventario == 1 || intentarNuevo == 1) {
                        inventario.mostrarInventario();
                    } else if (opcionInventario == 2 || intentarNuevo == 2) {

                        EstacionesCombustible estaciongregado = inventario.agregarInventario(nombreEstacion, tipoCombustible, precioCombustible, cantidadCombustible);

                        if (estaciongregado != null) {
                            estacionesCombustibles.add(estaciongregado);
                        }
                    } else if (opcionInventario == 3 || intentarNuevo == 3) {
                        System.out.print("Ingresar el nombre de la estacion para eliminar: ");
                        nombreEstacionEliminar = br.readLine();
                        inventario.eliminarEstacionCombustible(nombreEstacionEliminar);
                    } else if (opcionInventario == 4 || intentarNuevo == 4) {
                        System.out.print("Ingresar el nombre de la estacion para modicar: ");
                        nombreEstacionModificar = br.readLine();
                        inventario.modficarEstacionCombustible(nombreEstacionModificar, nombreEstacion, tipoCombustible, precioCombustible, cantidadCombustible);
                    } else if (opcionInventario == 5 || intentarNuevo == 5) {
                        System.out.print("Ingresar el nombre de la estación a buscar: ");
                        nombreBuscar = br.readLine();
                        inventario.BuscarEstacion(nombreBuscar);
                    }
                    break;
                case 3:
                    do {
                        System.out.println();
                        System.out.print("¿Cuantos clientes hay?: ");
                        numeroClientes = Integer.parseInt(br.readLine());
                    } while (numeroClientes <= 0);
                    for (int contadorCleintes = 1; contadorCleintes <= numeroClientes; contadorCleintes++) {
                        System.out.println("\nCliente #" + contadorCleintes + "\n");
                        System.out.print("ingresar su nombre: ");
                        nombreCliente = br.readLine();
                        System.out.print("Ingresar edad: ");
                        clienteEdad = br.readLine();
                        System.out.print("Ingresar en codigo del cliente: ");
                        clienteCodigo = Integer.parseInt(br.readLine());
                        Cliente clientes = new Cliente(nombreCliente, clienteEdad, clienteCodigo);
                        cliente.add(clientes);
                    }
                    break;
                case 4:
                    BaseDatosCliente baseDatosCliente = new BaseDatosCliente(br, cliente);
                    do {
                        System.out.println("\n");
                        System.out.println("\t1) Mostrar datos del cliente ");
                        System.out.println("\t2) Agregar cliente ");
                        System.out.println("\t3) Eliminar cliente ");
                        System.out.println("\t4) Modificar datos del cliente");
                        System.out.println("\t5) Buscar datos del cliente");
                        System.out.print("\tElige un de las opciones (1-2-3-4-5): ");
                        opcionCliente = Integer.parseInt(br.readLine());
                    } while (opcionCliente <= 0);
                    if (opcionCliente != 1 && opcionCliente != 2 && opcionCliente != 3 && opcionCliente != 4 && opcionCliente != 5) {
                        do {
                            System.out.print("Opción incorrecta. Ingresar de nuevo: ");
                            entradaCliente = Integer.parseInt(br.readLine());

                        } while (entradaCliente <= 0 || entradaCliente > 6);
                    }
                    if (opcionCliente == 1 || entradaCliente == 1) {
                        baseDatosCliente.mostrarDatosCliente();
                    } else if (opcionCliente == 2 || entradaCliente == 2) {

                        Cliente clienteAgregado = baseDatosCliente.agregarCliente(nombreEstacion, tipoCombustible, cantidadCombustible);

                        if (clienteAgregado != null) {
                            cliente.add(clienteAgregado);
                        }
                    } else if (opcionCliente == 3 || entradaCliente == 3) {
                        System.out.print("Ingresar el nombre del cliente para eliminar: ");
                        nombreClienteEliminar = br.readLine();
                        baseDatosCliente.eliminarCLiente(nombreClienteEliminar);
                    } else if (opcionCliente == 4 || entradaCliente == 4) {
                        System.out.print("Ingresar el nombre del cliente que quiere modifcar: ");
                        nombreClienteModificar = br.readLine();
                        baseDatosCliente.modificarCliente(nombreClienteModificar, nombreCliente, clienteEdad, clienteCodigo);
                    } else if (opcionCliente == 5 || entradaCliente == 5) {
                        System.out.print("Ingresar el nombre de cliente que quiere buscar: ");
                        clienteBuscar = br.readLine();
                        baseDatosCliente.buscarCliente(clienteBuscar);
                    }
                    break;

                case 5:

                    BaseDatosCliente baseDatosCliente1 = new BaseDatosCliente(br, cliente);
                    Inventario baseDatosInventaria = new Inventario(br, estacionesCombustibles);
                    BaseDatosVenta baseDatosVenta = new BaseDatosVenta(br, cliente);

                    do {
                        System.out.println("\n");
                        System.out.println("\t1) REGISTRAR VENTA ");
                        System.out.println("\t2) REPORTE VENTA ");
                        System.out.print("\tElige una de las opciones(1-2)");
                        opcionVenta = Integer.parseInt(br.readLine());
                    } while (opcionVenta <= 0);
                    if (opcionVenta != 1 && opcionVenta != 2) {
                        do {
                            System.out.print("Opción incorrecta. Ingresar de nuevo: ");
                            entradaVenta = Integer.parseInt(br.readLine());

                        } while (entradaVenta <= 0 || entradaVenta > 2);
                    }
                    if (opcionVenta == 1 || entradaVenta == 1) {

                        System.out.print("Ingresar el codigo del cliente: ");
                        codigoClienteBuscar = Integer.parseInt(br.readLine());

                        Cliente clienteEnContrado = baseDatosCliente1.buscarClientePorCodigo(codigoClienteBuscar);
                        if (clienteEnContrado == null) {
                            System.out.println("\nNO EXISTE CLIENTE, TERMINA PROCESO");
                            break;
                        }
                        if (clienteEnContrado != null) {
                            System.out.println("Si se encontro la id");
                        }


                        System.out.print("Ingresar el nombre de la estación de combustible: ");
                        nombreCombustibleBuscar = br.readLine();

                        EstacionesCombustible combustibleEncontrado = baseDatosInventaria.BuscarEstacion2(nombreCombustibleBuscar);

                        if (combustibleEncontrado == null) {
                            System.out.println("NO EXITE EN LA ESTACION DE  INVENTARIO. termina proceso");
                            break;
                        }
                        if (clienteEnContrado != null) {
                            System.out.println("\nSi se encontro la estación");
                        }


                        System.out.print("ingresar la cantidad de combustible a comprar ");
                        cantidadCombustibleBuscar = Integer.parseInt(br.readLine());
                        do {
                            System.out.print("INGRESAR dia de venta(1-2-3-4-5-6-7)");
                            diaventa = Integer.parseInt(br.readLine());
                        } while (diaventa < 1 || diaventa >= 7);
                        System.out.println("Ingresar el tipo de comprobante: ");
                        tipocomprobante = br.readLine();


                        boolean validaStock = baseDatosInventaria.validaStock(combustibleEncontrado, cantidadCombustibleBuscar);

                        if (!validaStock) {
                            System.out.println("CANTIDAD CONBUSTIBLE INVALIDO");
                            break;
                        }
                        VentaCombustible venta = baseDatosVenta.agregarVenta(clienteEnContrado, combustibleEncontrado, cantidadCombustibleBuscar, diaventa, tipocomprobante);
                        if (venta != null) {
                            ventas.add(venta);
                        }
                    } else if (opcionVenta == 2 || entradaVenta == 2) {

                        baseDatosVenta.reporteVentaPorNombreCombustible(nombreCombustibleBuscar);
                        baseDatosVenta.reporteVentaPordia(diaventa);
                    }

                    break;

                case 6:
                    BaseDatosVenta baseDatosVenta2 = new BaseDatosVenta();


                    System.out.println("INGRESAR DIA VENTA");
                    int DIAVENTA = Integer.parseInt(br.readLine());

                    System.out.println("INGRESAR NOMBRE CONMBUSTUBLE");
                    String nombreCombustible = (br.readLine());


                    System.out.println("MOSTRAR REPORTE");
                    baseDatosVenta2.reporteVentaPordia(DIAVENTA);
                    baseDatosVenta2.reporteVentaPorNombreCombustible(nombreCombustible);

            }
        } while (opcion != 7);
    }

}

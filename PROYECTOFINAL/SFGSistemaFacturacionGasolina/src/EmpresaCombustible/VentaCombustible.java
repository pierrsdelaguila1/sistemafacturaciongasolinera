package EmpresaCombustible;

public class VentaCombustible {
    Cliente cliente;
    EstacionesCombustible estacionesCombustible;
    int cantidad;
    int dia;
    String tipoComprobante;

    public VentaCombustible() {
        cantidad = 0;
        dia = 0;
        tipoComprobante = "";
    }

    public VentaCombustible(Cliente cliente, EstacionesCombustible estacionesCombustiblem, int cantidad, int dia, String tipoComprante) {
        this.cliente = cliente;
        this.estacionesCombustible = estacionesCombustiblem;
        this.cantidad = cantidad;
        this.dia = dia;
        this.tipoComprobante = tipoComprante;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public EstacionesCombustible getEstacionesCombustible() {
        return estacionesCombustible;
    }

    public void setEstacionesCombustible(EstacionesCombustible estacionesCombustible) {
        this.estacionesCombustible = estacionesCombustible;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getDia() {
        return dia;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }

    public String getTipoComprobante() {
        return tipoComprobante;
    }

    public void setTipoComprobante(String tipoComprobante) {
        this.tipoComprobante = tipoComprobante;
    }
}
